﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace QueryInterception
{


    /// <summary>
    /// used to have some action run when the variable is about to go out of scope
    /// </summary>
    public class Disposable : IDisposable
    {
        private readonly Action _onDispose;
        private bool _isDisposed;
        public Disposable(Action onDispose)
        {
            if (onDispose == null)
                throw new ArgumentNullException("onDispose");
            _onDispose = onDispose;
        }
        public void Dispose()
        {
            if (_isDisposed)
                return;
            _isDisposed = true;
            _onDispose();
        }
    }

}
