﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace System
{
    public static class StringExtensions
    {
        public static bool IsNullOrEmpty(this string s)
        {
            return string.IsNullOrEmpty(s);
        }
        public static bool HasValue(this string s)
        {
            return s.IsNullOrEmpty() == false;
        }
        public static string Delimit(this IEnumerable<string> data, string delimiter=",")
        {
            return data.Aggregate((s1, s2) => s1 + delimiter + s2);
        }
    }
}