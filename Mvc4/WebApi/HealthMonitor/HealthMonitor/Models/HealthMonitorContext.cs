﻿using System.Data.Entity;

namespace HealthMonitor.Models
{
    public class HealthMonitorContext : DbContext
    {
        // You can add custom code to this file. Changes will not be overwritten.
        // 
        // If you want Entity Framework to drop and regenerate your database
        // automatically whenever you change your model schema, add the following
        // code to the Application_Start method in your Global.asax file.
        // Note: this will destroy and re-create your database with every model change.
        // 
        // System.Data.Entity.Database.SetInitializer(new System.Data.Entity.DropCreateDatabaseIfModelChanges<HealthMonitor.Models.HealthMonitorContext>());

        public HealthMonitorContext()
            : base("name=HealthMonitorContext")
        {
            this.Configuration.LazyLoadingEnabled = false;
            this.Configuration.ProxyCreationEnabled = false; //http://social.msdn.microsoft.com/Forums/en/wcf/thread/a5adf07b-e622-4a12-872d-40c753417645

        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //modelBuilder.Entity<Nvironment>()
            //    .HasMany(f => f.Sites)
            //    .WithOptional(f => f.Nvironment);

            //modelBuilder.Entity<Site>()
            //    .HasOptional(s => s.Nvironment)
            //    .WithOptionalDependent()
            //    .WillCascadeOnDelete(false);
            //    .WithMany(f=>f.Sites)
            //    .HasForeignKey(f=>f.EnvironmentId);

            base.OnModelCreating(modelBuilder);
        }

        public DbSet<Site> Sites { get; set; }

        public DbSet<Nvironment> Environments { get; set; }
    }
}
