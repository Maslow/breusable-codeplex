﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using Antlr.Runtime;
using Antlr4.StringTemplate;

namespace ParserConsole
{
	class Program
	{
		const string defaultCalcInput = "1+2-3*4/4+1";
		static bool useInteractiveCalc = false;

		static void Main(string[] args)
		{
			var output = HelloWorld();
			Console.WriteLine("HelloWorld" + Environment.NewLine);
			Console.WriteLine(output);
			Console.ReadKey();
			Console.Clear();

			Console.WriteLine("T4" + Environment.NewLine);
			Console.WriteLine(HelloT4());
			Console.ReadKey();
			Console.Clear();

			Console.WriteLine("Calculator");
			CalcToConsole();
			
			Console.WriteLine("Press any key to exit");
			Console.ReadKey();
		}

		private static void CalcToConsole()
		{
			if (useInteractiveCalc)
			{
				Console.WriteLine("Enter Expression followed by a line with just ctrl-z");
				using (var input = Console.OpenStandardInput())
				{
					var stream = new ANTLRInputStream(input);
					Console.WriteLine(RunCalculator(stream));
				}

			}
			else
			{
				Console.WriteLine(defaultCalcInput);
				Console.WriteLine(RunCalculator(new ANTLRStringStream(defaultCalcInput)));
			}
		}

		/// <summary>
		/// http://programming-pages.com/2012/06/28/antlr-with-c-a-simple-grammar/
		/// </summary>
		/// <param name="input"></param>
		private static int RunCalculator(ICharStream input)
		{
			
			var lexer = new CalculatorLexer(input);
			var tokens = new CommonTokenStream(lexer);
			var parser = new CalculatorParser(tokens);
			return parser.addSubExpr();
		}

		static string HelloT4()
		{

			var path = @"TextTemplate1.tt";
			Debug.Assert(System.IO.File.Exists(path));
			var file = new FileInfo(path);

			using (var stream = File.Open(file.FullName, FileMode.Open))
			{
				var input = new ANTLRInputStream(stream);
				var lexer = new T4Lexer(input);
				//new T4Lexer((ICharStream)input);
				var tokens = new CommonTokenStream(lexer);
				var parser = new T4Parser(tokens);
				return parser.parse().Tree.ToStringTree();
				//return parser.TokenNames.Aggregate((s1, s2) => s1 + "," + s2); 



			}

		}

		/// <summary>
		/// http://blog.newslacker.net/2011/10/stringtemplate-4-net-c-getting-started.html
		/// </summary>
		/// <returns></returns>
		static string HelloWorld()
		{
			Template template;

			//var path = @"C:\templates\template.st";
			var path=@"TextFile1.st";
			Debug.Assert(System.IO.File.Exists(path));
			var file = new FileInfo(path);
			using (StreamReader reader = file.OpenText())
				template = new Template(reader.ReadToEnd(), '$', '$'); // custom delimeter (defaults are "<" and ">")

			template.Add("string", "Hello World!");
			template.Add("array", new string[] { "Record A", "Record B", "Record C" });
			template.Add("object", new { PropertyA = "A", PropertyB = "B" });

			return template.Render();
		}

		
	}
}
