﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HealthMonitor.Models;

namespace HealthMonitor.MvcControllers
{
    public class EnvironmentController : Controller
    {
        private HealthMonitorContext db = new HealthMonitorContext();

        //
        // GET: /Environment/

        public ActionResult Index()
        {
            return View(db.Environments.ToList());
        }

        //
        // GET: /Environment/Details/5

        public ActionResult Details(int id = 0)
        {
            Nvironment nvironment = db.Environments.Find(id);
            if (nvironment == null)
            {
                return HttpNotFound();
            }
            return View(nvironment);
        }

        //
        // GET: /Environment/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Environment/Create

        [HttpPost]
        public ActionResult Create(Nvironment nvironment)
        {
            if (ModelState.IsValid)
            {
                if (db.Environments.Any(e => e.Name == nvironment.Name))
                {
                    ModelState.AddModelError("Name", "Name must be unique");
                    return View(nvironment);
                }
                db.Environments.Add(nvironment);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(nvironment);
        }

        //
        // GET: /Environment/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Nvironment nvironment = db.Environments.Find(id);
            if (nvironment == null)
            {
                return HttpNotFound();
            }
            if(nvironment.Sites==null)
                nvironment.Sites=new Collection<Site>();
            return View(nvironment);
        }

        //
        // POST: /Environment/Edit/5

        [HttpPost]
        public ActionResult Edit(Nvironment nvironment)
        {
            if (ModelState.IsValid)
            {
                db.Entry(nvironment).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(nvironment);
        }

        //
        // GET: /Environment/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Nvironment nvironment = db.Environments.Find(id);
            if (nvironment == null)
            {
                return HttpNotFound();
            }
            return View(nvironment);
        }

        //
        // POST: /Environment/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Nvironment nvironment = db.Environments.Find(id);
            db.Environments.Remove(nvironment);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}