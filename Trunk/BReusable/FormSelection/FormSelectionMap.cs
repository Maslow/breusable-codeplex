﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace BReusable
{
	/// <summary>
	/// Shortcuts to the most used areas of FormSelection
	/// </summary>
	public static class FormSelectionMap
	{
		public static String TextBoxInput(this String defaultText,String title,int maxLength,bool multiLine,Func<String,bool> validator)
	{
		return FormSelection.FormSelection.TextBoxInput(title, defaultText, maxLength, multiLine, validator);
	}
		public static TClass ListBoxSelect<TClass>(this IEnumerable<TClass> values, String title) where TClass : class
		{
			return FormSelection.FormSelection.ListBoxSelect(values, title);
		}
		public static TClass ComboBoxSelect<TClass>(this IEnumerable<TClass> values, String title) where TClass : class
		{
			return FormSelection.FormSelection.ComboBoxSelect(values, title);
		}

		public static List<TClass> CheckedListBoxSelect<TClass>(this IEnumerable<TClass> values, String title)
			where TClass:class
		{
			return FormSelection.FormSelection.CheckedListBoxSelect(values, title);
		}

		public static List<TClass> CheckedListBoxSelect<TClass>(this IEnumerable<TClass> values, String title
			, Func<TClass, String> customToString)
		{
			var tuples = new List<Pair<String, TClass>>();
			values.ForEach(item => tuples.Add(new Pair<string, TClass>(customToString(item), item,()=>customToString(item))));
			var uiResult = FormSelection.FormSelection.CheckedListBoxSelect(tuples, title);
			if (uiResult == null)
				return null;
			var result = new List<TClass>();
			uiResult.ForEach(item => result.Add(item.Value2));
			return result;
		}
		public static T? ListBoxSelectEnum<T>(String title) where T : struct
		{
			return FormSelection.FormSelection.ListBoxSelectEnum<T>(title);
		}
		public static T? ComboBoxSelectEnum<T>(String title) where T : struct
		{
			return FormSelection.FormSelection.ComboBoxSelectEnum<T>(title);
		}


		public static FormSelection.FormSelection InputBoxForm(TextBox textBox)
		{
			return new FormSelection.FormSelection(textBox);
		}
		public static FormSelection.FormSelection ComboBoxForm(ComboBox comboBox)
	{
		return new FormSelection.FormSelection(comboBox);
	}


	
		/// <summary>
		/// If you need to change the form around, make the size bigger, make it maximize, anything else
		/// </summary>
		/// <param name="listBox"></param>
		/// <returns></returns>
		public static FormSelection.FormSelection ListBoxform(ListBox listBox)
		{
			return new FormSelection.FormSelection(listBox);
		}



	}
}
