﻿using System.ComponentModel.Composition;
using System.Windows.Media;
using Microsoft.VisualStudio.Text.Classification;
using Microsoft.VisualStudio.Utilities;

namespace T4EditorClassifier
{
    #region Format definition
    /// <summary>
    /// Defines an editor format for the T4EditorClassifier type that has a purple background
    /// and is underlined.
    /// </summary>
    [Export(typeof(EditorFormatDefinition))]
    [ClassificationType(ClassificationTypeNames = "T4EditorCommentClassifier")]
    [Name("T4EditorCommentClassifier")]
    [UserVisible(true)] //this should be visible to the end user
    [Order(Before = Priority.Default)] 
    internal sealed class T4EditorCommentClassifierFormat : ClassificationFormatDefinition
    {
        /// <summary>
        /// Defines the visual format for the "T4EditorClassifier" classification type
        /// </summary>
        public T4EditorCommentClassifierFormat()
        {
            this.DisplayName = "T4EditorCommentClassifier"; //human readable version of the name
            this.ForegroundColor=Colors.Green;
            //this.TextDecorations = System.Windows.TextDecorations.Underline;
        }
    }
    #endregion //Format definition
}
