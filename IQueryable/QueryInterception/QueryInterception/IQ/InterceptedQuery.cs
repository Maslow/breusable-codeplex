﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;

namespace QueryInterception
{

    internal class InterceptedQuery<T> : IOrderedQueryable<T>
    {
        private Expression _expression;
        protected internal readonly InterceptingProvider _provider;

        internal InterceptedQuery(
           InterceptingProvider provider,
           Expression expression)
        {
            this._provider = provider;
            this._expression = expression;
        }
        public IEnumerator<T> GetEnumerator()
        {
            return this._provider.ExecuteQuery<T>(this._expression);
        }
        IEnumerator IEnumerable.GetEnumerator()
        {
            return this._provider.ExecuteQuery<T>(this._expression);
        }
        public Type ElementType
        {
            get { return typeof(T); }
        }
        public Expression Expression
        {
            get { return this._expression; }
        }
        public IQueryProvider Provider
        {
            get { return this._provider; }
        }
    }
}
