﻿using System;
using System.Xml.Linq;

namespace BReusable.PivotalTracker
{
	public class PivotalTrackerStory
	{
		public int Id { get;private set; }
		public PivotalTrackerAPI.StoryType StoryType { get; private set; }
		public string Url { get; private set; }
		public string CurrentState { get; private set; }
		public string Description { get; private set; }
		public string Name { get; private set; }
		public string RequestedBy { get; private set; }
		//public DateTime Created_At { get; private set; }
		internal PivotalTrackerStory(XContainer xmlString)
		{
			Id = int.Parse(xmlString.Element("id").ThrowIfNull().Value);
			StoryType =(PivotalTrackerAPI.StoryType) Enum.Parse(typeof(PivotalTrackerAPI.StoryType),
				xmlString.Element("story_type").ThrowIfNull().Value);
			Url = xmlString.Element("url").ThrowIfNull().Value;
			CurrentState = xmlString.Element("current_state").ThrowIfNull().Value;
			Description = xmlString.Element("description").ThrowIfNull().Value;
			Name = xmlString.Element("name").ThrowIfNull().Value;
			RequestedBy = xmlString.Element("requested_by").ThrowIfNull().Value;
			//Created_At =DateTime.Parse( xmlString.Element("created_at").Value);

		}
	}
}
