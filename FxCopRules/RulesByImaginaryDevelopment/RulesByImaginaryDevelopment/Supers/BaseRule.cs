﻿using Microsoft.FxCop.Sdk;

public abstract class BaseRule : BaseIntrospectionRule // I believe this has to be in the default namespace
    {
        protected BaseRule(string ruleName)
            : base(ruleName,
                //typeof(BaseRule).Assembly.GetName().Name+".Rules",
                "RulesByImaginaryDevelopment.RuleMetadata",
                typeof(BaseRule).Assembly) { }

    }

