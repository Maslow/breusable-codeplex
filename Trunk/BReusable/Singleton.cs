﻿using System;

namespace BReusable
{
	/// <summary>
	/// Immutable singleton with ToString override functionality
	/// </summary>
	/// <typeparam name="T"></typeparam>
	public class Singleton<T>
	{
		public T Value { get; private set; }
		protected readonly Func< String> ToStringFromType;
		public Singleton(T value)
		{
			Value = value;

		}
		public Singleton(T value, Func<T, String> toStringFunc):this(value)
		{
			if (toStringFunc!=null)
			ToStringFromType =()=> toStringFunc(Value);
		}
		public Singleton(T value, Func<String> toStringfunc):this(value)
		{
			ToStringFromType = toStringfunc;	
		}

		public override string ToString()
		{
			return (ToStringFromType == null ? base.ToString() : ToStringFromType());
		}
	}
	
}
