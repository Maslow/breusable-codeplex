﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Mvc4.Models
{
    public class ShowModel
    {
        [Key]
        [HiddenInput]
        public int ShowID { get; set; }
        public string ShowName { get; set; }
        [DataType(DataType.Url)]
        //[System.ComponentModel.DataAnnotations.UIHint(
        public string Imdb { get; set; }
    }
}