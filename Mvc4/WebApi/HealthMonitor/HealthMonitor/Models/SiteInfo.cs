﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;

namespace HealthMonitor.Models
{
    public class SiteInfo
    {
        public Site Site { get; set; }
        public IEnumerable<KeyValuePair<string, string>> Status { get; set; }
// ReSharper disable UnusedMember.Local //used by serialization
        private SiteInfo() { }
// ReSharper restore UnusedMember.Local
        public SiteInfo(Site site, IEnumerable<KeyValuePair<string,string>> status)
        {
            Site = site;
            Status = status;
        }
    }
}