﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Orchard.Utility.Extensions
{
    public static class EnumerableExtensions
    {
        public static IEnumerable<T> Maybe<T>(this IEnumerable<T> source ) {
            return source ?? Enumerable.Empty<T>();
        }
    }
}
