﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Text.RegularExpressions;
using System.Management;


namespace BReusable
{
	public enum FileSaveMode
	{
		Overwrite, Append, ThrowExceptionIfExists
	}
}

public static class StringExtensions
{
	
	public static string NetSendToSelf(this string text)
	{
		
		Process.Start("net", " send" + Environment.MachineName + " " + text);
		return text;
	}
public static int? ToNullableIntegerFromTryParse(this string text)
{
	int val;
	if (int.TryParse(text, out val))
		return val;
	else
		return null;
}

	/// <summary>
	/// Join a list of strings with a separator
	/// From BReusable
	/// </summary>
	/// <param name="l"></param>
	/// <param name="separator"></param>
	/// <returns></returns>
	public static String Join(this IEnumerable<string> l, string separator)
	{
		var counter = 0;

		var result = new StringBuilder();
		foreach (var item in l)
		{
			if (counter != 0) result.Append(separator);
			result.Append(item);
			counter++;
		}
		return result.ToString();
	}

	/// <summary>
	/// From BReusable
	/// </summary>
	/// <param name="list"></param>
	/// <param name="seperator"></param>
	/// <param name="startIndex"></param>
	/// <param name="length"></param>
	/// <returns></returns>
	public static String Join(this IList<String> list, string seperator, int startIndex, int? length)
	{
		if (length.HasValue == false) length = list.Count - startIndex;
		var result = new StringBuilder();
		for (int i = startIndex; i < length; i++)
		{
			if (i != startIndex) result.Append(seperator);
			result.Append(list[i]);
		}
		return result.ToString();
	}


	/// <summary>
	/// From BReusable
	/// </summary>
	/// <param name="text"></param>
	/// <returns></returns>
	public static byte[] ToByteArray(this String text)
	{
		var encoding = new ASCIIEncoding();
		return encoding.GetBytes(text);
	}

	/// <summary>
	/// From BReusable
	/// </summary>
	/// <param name="buffer"></param>
	/// <returns></returns>
	public static String ToStringFromBytes(this byte[] buffer)
	{
		var encoding = new ASCIIEncoding();
		return encoding.GetString(buffer);
	}



	/// <summary>
	/// From BReusable
	/// </summary>
	/// <param name="text"></param>
	/// <returns></returns>
	public static String RemoveTabs(this String text)
	{
		return Regex.Replace(text, "\t", String.Empty);

	}

	/// <summary>
	/// From BReusable
	/// </summary>
	/// <param name="output"></param>
	/// <returns></returns>
	public static String ToDebug(this string output)
	{
		System.Diagnostics.Debug.WriteLine(output);
		return output;
	}

	/// <summary>
	/// From BReusable
	/// </summary>
	/// <param name="output"></param>
	/// <returns></returns>
	public static String ToConsole(this string output)
	{
		Console.WriteLine(output);
		return output;
	}


	public static void ToFile(this string output, string path,BReusable. FileSaveMode saveMode)
	{
		if (saveMode == BReusable.FileSaveMode.ThrowExceptionIfExists && System.IO.File.Exists(path))
			throw new ArgumentException("File already exists:" + path);

		using (var outFile = new System.IO.StreamWriter(path, saveMode == BReusable.FileSaveMode.Append ? true : false))
		{
			outFile.Write(output);
		}
	}

	/// <summary>
	/// From BReusable
	/// </summary>
	/// <param name="text"></param>
	/// <param name="pattern"></param>
	/// <param name="ignoreCase"></param>
	/// <returns></returns>
	public static bool IsMatch(this string text, String pattern, bool ignoreCase)
	{
		return ignoreCase ? Regex.IsMatch(text, pattern, RegexOptions.IgnoreCase) :
			Regex.IsMatch(text, pattern);
	}

	/// <summary>
	/// Uses System.Management
	/// </summary>
	/// <param name="sFilePath"></param>
	/// <returns></returns>
	public static string GetUniversalName(this string sFilePath)
	{

		if (sFilePath == string.Empty || sFilePath.IndexOf(":") > 1)

			return sFilePath;

		if (sFilePath.StartsWith("\\"))
		{
			return (new Uri(sFilePath)).ToString();
		}

		var searcher = new
		ManagementObjectSearcher(@"SELECT RemoteName FROM win32_NetworkConnection WHERE LocalName = '"
		+ sFilePath.Substring(0, 2) + "'");

		foreach (ManagementObject managementObject in searcher.Get())
		{
			var sRemoteName = managementObject["RemoteName"] as string;
			sRemoteName += sFilePath.Substring(2);
			return (new Uri(sRemoteName)).ToString();
		}

		return sFilePath;

	}

	/// <summary>
	/// Uses System.Management
	/// </summary>
	/// <param name="sFilePath"></param>
	/// <returns></returns>
	public static string GetUniversalPath(this string sFilePath)
	{

		if (sFilePath.IsNullOrEmpty() || sFilePath.StartsWith("\\"))
			return sFilePath;

		var searcher = new ManagementObjectSearcher(
		@"SELECT RemoteName FROM win32_NetworkConnection WHERE LocalName = '"
		+ sFilePath.Substring(0, 2) + "'");

		foreach (ManagementObject managementObject in searcher.Get())
		{
			var sRemoteName = managementObject["RemoteName"] as string;
			sRemoteName += sFilePath.Substring(2);
			return sRemoteName;
		}

		return sFilePath;

	}

}

