﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Mvc4.Models
{
    public class EpisodeModel
    {
        [Key]
        [HiddenInput]
        public int EpisodeID { get; set; }
        public int ShowID { get; set; }
        public DateTime? LastWatched { get; set; }
        public string EpisodeName { get; set; }
        public byte? EpisodeNumber { get; set; }
        public byte? Season { get; set; }
        public string Path { get; set; }

    }
}