﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BReusable.T4
{
    public class RuleViolation
    {
        public string Message { get; private set; }
        public string ViolatorName { get; private set; }
        public RuleViolation(string message, string violatorName)
        {
            Message = message;
            ViolatorName = violatorName;

        }
    }
}
