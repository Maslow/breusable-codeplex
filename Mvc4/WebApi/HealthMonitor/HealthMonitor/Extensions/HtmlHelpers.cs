﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HealthMonitor.Extensions
{
    public static class HtmlHelpers
    {
        public static string Serialize(this HtmlHelper helper, object value) { return Newtonsoft.Json.JsonConvert.SerializeObject(value); }
        public static string SerializeAttribute(this HtmlHelper helper, object value) { return HttpUtility.HtmlAttributeEncode(helper.Serialize(value)); }
    }
}