﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using QueryInterception;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace DataAccess.CodeFirst
{
    public class CodeFirstContext:DbContext,
        IRepository<Config>
    {
        public CodeFirstContext()
            : base("CodeFirstContext")
        {
            Database.SetInitializer<CodeFirstContext>(null);
        }
        public CodeFirstContext(string nameOrConnectionString):base(nameOrConnectionString) { }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //Database.SetInitializer(null);
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            base.OnModelCreating(modelBuilder);

        }

        protected DbSet<Config> Configs { get; set; }


        IQueryable<Config> IRepository<Config>.Query()
        {

            IQueryable < Config > q = from i in ((IObjectContextAdapter)this).ObjectContext.CreateObjectSet<Config>()
                                      select i;

            return InterceptingProvider.Intercept(q, new ExpressionWriter(new DebugTextWriter()));
        }
    }
}
