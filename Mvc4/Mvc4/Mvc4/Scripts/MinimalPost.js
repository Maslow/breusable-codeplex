﻿
$(document).ready(function () {
    var form = $('form');
    form.on('submit', function () {
        $('div.editor-field[data-value]', form).each(function (i, e) {
            var oldValue = $(e).attr('data-value');
            var currentEditor = $('input,select,textarea');
            if (currentEditor.val() == oldValue)
                currentEditor.attr('disabled', 'disabled');
        });

    });
});