﻿namespace LandingSite.Core
open System.ComponentModel
open System.ComponentModel.DataAnnotations
open DataAnnotationsExtensions
type ForgotPasswordModel = {
        [<Required>]
        [<Email(ErrorMessage="{0} is not a valid email address.")>]
        EmailAddress:string

        [<Required>]
        UserName:string

        [<Required>]
        [<ReadOnly(true)>]
        ChallengeQuestion:string

        [<Required>]
        [<ReadOnly(true)>]
        ChallengeAnswer:string
    }
    

