﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace T4EditorClassifier.Tests
{
    [TestClass]
    public class EnumerableExtensionsTests
    {
        [TestMethod]
        public void DelimitBy_One_ReturnsInput()
        {
            var expected="test";
            var input = new[] {expected  };
            var actual = input.DelimitBy("abcdefg");
            Assert.AreEqual(expected, actual);

        }

        [TestMethod]
        public void DelimitBy_Two_ReturnsDelimited()
        {
            var expected = "test";
            var delimiter = "|";
            var input = new[] { expected,expected };
            var actual = input.DelimitBy(delimiter);
            Assert.AreEqual(expected + delimiter + expected, actual);

        }

        [TestMethod]
        public void DelimitBy_Three_ReturnsDelimited()
        {
            var expected = "test";
            var delimiter = "|";
            var input = new[] { expected, expected,expected };
            var actual = input.DelimitBy(delimiter);
            Assert.AreEqual(expected + delimiter + expected+delimiter+expected, actual);

        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void DelimitBy_None_Throws()
        {
            var expected = string.Empty;
            var input = new string[] {  };
            var actual = input.DelimitBy("abcdefg");
            Assert.AreEqual(expected, actual);

        }

        
    }
}
