﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Media;
using Roslyn.Compilers;
using Roslyn.Compilers.Common;
using Roslyn.Compilers.CSharp;
using Roslyn.Services;
using Roslyn.Services.Editor;

namespace SortMembers
{
    //metaprogramming meap pg 304 : 10.3.8
    //http://metadotnetbook.codeplex.com
    class AutoArrangeCodeAction : ICodeAction
    {
        private IDocument document;
        private TypeDeclarationSyntax token;

        public AutoArrangeCodeAction(IDocument document, TypeDeclarationSyntax token)
        {
            this.document = document;
            this.token = token;
        }

        public string Description
        {
            get { return "Auto-arrange"; }
        }

        public CodeActionEdit GetEdit(CancellationToken cancellationToken)
        {
           
            var captureRewriter = new AutoArrangeCaptureWalker();
            captureRewriter.VisitTypeDeclaration(this.token);
            var result = new AutoArrangeReplaceRewriter(captureRewriter).VisitTypeDeclaration(this.token);

            var tree = (SyntaxTree)this.document.GetSyntaxTree(cancellationToken);
            var newTree = tree.GetRoot(cancellationToken).ReplaceNode(this.token, result);
            return new CodeActionEdit(document.UpdateSyntaxRoot(newTree));
          
        }

        public ImageSource Icon
        {
            get { return null; }
        }
    }
}
