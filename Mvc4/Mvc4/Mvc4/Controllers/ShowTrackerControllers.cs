﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Mvc4.Models;
using Mvc4.Models.EF;

namespace Mvc4.Controllers
{
	//classElement!=null? True
//found property:ShowID
//found attribute:System.ComponentModel.DataAnnotations.KeyAttribute
//found attribute:System.Web.Mvc.HiddenInputAttribute
//found property:ShowName
//found property:Imdb
//found attribute:System.ComponentModel.DataAnnotations.DataTypeAttribute

	public partial class ShowController:Controller
	{
		Mvc4Context db=new Mvc4Context();
		
		[AllowAnonymous]
        virtual public ActionResult Index(int? page,int? pageSize)
        {
			 if (page.HasValue&& page.Value>0)
                page -=1;
            else 
			page=0;
			
			PopulateViewBag();
			
			pageSize=pageSize??30;
			var items=db.Shows;
			var length=items.Count();
			ViewBag.PageSize=pageSize;
			
			ViewBag.Page=page.Value+1;
			var model=items.ToArray().Skip((page.Value)*pageSize.Value).Take(pageSize.Value);
			ViewBag.Pages=length/pageSize+1;

			return View(model);
		}

		virtual public ActionResult Create()
        {
			
			PopulateViewBag();
			ViewBag.Change="Create";
            return View(new ShowModel());
        }
		partial void PopulateViewBag();
		

		[Authorize(Roles = "Admin")]
        [HttpPost]
        virtual public ActionResult Create(ShowModel model)
        {
			
            try
            {
                
                
                db.CreateShow(model);
                return RedirectToAction("Index");
            }
            catch
            {
				PopulateViewBag();
				ViewBag.Change="Create";
                return View(model);
            }
        }

		virtual public ActionResult Edit(int id)
        {
			
			var model=db.Shows.FirstOrDefault(f=>f.ShowID==id);
			PopulateViewBag();
			ViewBag.Change="Edit";
            return View("Create",model);
        }

		[Authorize(Roles="Admin")]
		[HttpPost]
        virtual public ActionResult Edit(int id,ShowModel model)
        {
			
			model.ShowID=id;
            try
            {
				db.UpdateShow(model,Request.Form.AllKeys);

                return RedirectToAction("Index");
            }
            catch
            {
				ViewBag.Change="Edit";
				PopulateViewBag();
                return View("Create",model);
            }
        }

	}

	//classElement!=null? True
//found property:EpisodeID
//found attribute:System.ComponentModel.DataAnnotations.KeyAttribute
//found attribute:System.Web.Mvc.HiddenInputAttribute
//found property:ShowID
//found property:LastWatched
//found property:EpisodeName
//found property:EpisodeNumber
//found property:Season
//found property:Path

	public partial class EpisodeController:Controller
	{
		Mvc4Context db=new Mvc4Context();
		
		[AllowAnonymous]
        virtual public ActionResult Index(int? page,int? pageSize)
        {
			 if (page.HasValue&& page.Value>0)
                page -=1;
            else 
			page=0;
			
			PopulateViewBag();
			
			pageSize=pageSize??30;
			var items=db.Episodes;
			var length=items.Count();
			ViewBag.PageSize=pageSize;
			
			ViewBag.Page=page.Value+1;
			var model=items.ToArray().Skip((page.Value)*pageSize.Value).Take(pageSize.Value);
			ViewBag.Pages=length/pageSize+1;

			return View(model);
		}

		virtual public ActionResult Create()
        {
			
			PopulateViewBag();
			ViewBag.Change="Create";
            return View(new EpisodeModel());
        }
		partial void PopulateViewBag();
		

		[Authorize(Roles = "Admin")]
        [HttpPost]
        virtual public ActionResult Create(EpisodeModel model)
        {
			
            try
            {
                
                
                db.CreateEpisode(model);
                return RedirectToAction("Index");
            }
            catch
            {
				PopulateViewBag();
				ViewBag.Change="Create";
                return View(model);
            }
        }

		virtual public ActionResult Edit(int id)
        {
			
			var model=db.Episodes.FirstOrDefault(f=>f.EpisodeID==id);
			PopulateViewBag();
			ViewBag.Change="Edit";
            return View("Create",model);
        }

		[Authorize(Roles="Admin")]
		[HttpPost]
        virtual public ActionResult Edit(int id,EpisodeModel model)
        {
			
			model.EpisodeID=id;
            try
            {
				db.UpdateEpisode(model,Request.Form.AllKeys);

                return RedirectToAction("Index");
            }
            catch
            {
				ViewBag.Change="Edit";
				PopulateViewBag();
                return View("Create",model);
            }
        }

	}

		
}
﻿
 
