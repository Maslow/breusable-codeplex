﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DataAccess.CodeFirst;
using DataAccess.ModelFirst;
using QueryInterception;
using Config = DataAccess.CodeFirst.Config;

namespace Tester
{
    public partial class Form1 : Form
    {
        const string CodeFirst = "CodeFirst";
        const string CodeFirstGenerated = "CodeFirstGenerated";
        const string ModelFirst = "ModelFirst/DatabaseFirst";

        public Form1()
        {
            InitializeComponent();
            foreach(ConnectionStringSettings item in ConfigurationManager.ConnectionStrings)
            {
                comboBox2.Items.Add(item);
            }
            
            var items = new[]
                            {
                                CodeFirst,
                                CodeFirstGenerated,
                                //model first and database first
                                ModelFirst
                            };
            comboBox1.DataSource = items;
            comboBox1.SelectedIndex = 0;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            InterceptingProvider.DoTrace = true;
            if (!(comboBox1.SelectedItem is string))
                return;
            switch (comboBox1.SelectedItem.ToString())
            {
                case CodeFirstGenerated:
                    GetGeneratedCodeFirst();
                    break;
                case CodeFirst:
                    GetCodeFirst();
                    break;
                case ModelFirst:
                    GetModelFirst();
                    break;
                default:
                    break;
            }
           
        }

        private void GetGeneratedCodeFirst()
        {
            if(comboBox2.Text.Length>0)
            {
                using (var db = new CodeFirstContextGenerated(comboBox2.Text))
                {
                    var source = (IRepository<DataAccess.CodeFirst.Config>)db;
                    var q = source.Query().ToArray();
                    dataGridView1.DataSource = q.ToArray();

                }
            } else
            using(var db= new CodeFirstContextGenerated())
            {
                var source = (IRepository<DataAccess.CodeFirst.Config>)db;
                var q = source.Query().ToArray();
                dataGridView1.DataSource = q.ToArray();

            }
        }

        private void GetCodeFirst()
        {
           using(var db = new CodeFirstContext())
           {
               var source = (IRepository<DataAccess.CodeFirst.Config>) db;
               var q = source.Query().ToArray();
               dataGridView1.DataSource = q.ToArray();
           }
        }

        void GetModelFirst()
        {
            using (var db = new PaySpanConfigEntities())
            {
                var source = (IRepository<DataAccess.ModelFirst.ConfigModel>) db;
                var q = source.Query().ToArray();
                //var q = db.ConfigsQuery.ToArray();
                dataGridView1.DataSource = q.ToArray();
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e) { dataGridView1.DataSource = null; }
    }
}
