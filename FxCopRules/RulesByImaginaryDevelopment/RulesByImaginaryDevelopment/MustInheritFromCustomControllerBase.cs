﻿using Microsoft.FxCop.Sdk;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RulesByImaginaryDevelopment
{
    class MustInheritFromCustomControllerBase:BaseRule
    {
        public MustInheritFromCustomControllerBase() : base("MustInheritFromCustomControllerBase") { }
        public override ProblemCollection Check(TypeNode type)
        {
            if(type==FrameworkTypes.Object || type.BaseType==FrameworkTypes.Object || type.DeclaringModule.Name.StartsWith("System") || type.DeclaringModule.Name.StartsWith("Microsoft"))
                return Problems;
            if(type.BaseType.FullName=="System.Web.Mvc.Controller")
                Problems.Add(new Problem(new Resolution("Inherit from the custom controller"),type));
            return base.Check(type);
        }
    }
}
