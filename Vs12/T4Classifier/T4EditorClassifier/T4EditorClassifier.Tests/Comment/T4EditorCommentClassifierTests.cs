﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.Text;
using Microsoft.VisualStudio.Text.Classification;
using Rhino.Mocks;

namespace T4EditorClassifier.Tests.Comment
{
    /// <summary>
    /// A dual MSTest- NUnit Test supported test class.
    /// </summary>
    /// <remarks>
    /// MSTest: there are no categories, so we map Category to Description
    /// There is no RowTest (see DataDriven tests instead)
    /// NUnit: you can't use TestContext (mapped to object)
    /// </remarks>
    [TestClass]
    public partial class T4EditorCommentClassifierTests
    {

        [TestMethod]
        public void FindMatchingOpenChar_Match()
        {

            var s = ClassificationTestHelper.SetupSnapshot(" /*  */  ");
            var c = ClassificationTestHelper.SetupClassifier(r => new T4EditorCommentClassifier(r));
            var sp = new SnapshotPoint(s, s.GetText().IndexOf("*/"));
            SnapshotSpan pairSpan;
            var found = T4EditorCommentClassifier.FindMatchingOpenChar(sp, c, "/*", "*/", 2, out pairSpan);
            Assert.IsTrue(found);

        }

        [TestMethod]
        public void GetClassificationSpans_OpenNoCloseMultiLine_ReturnsOne()
        {
            var source = "/* \r\n";

            var actual = GetClassificationSpans(source);

            Assert.IsTrue(actual.Count == 1);
            Assert.AreEqual(source.Length, actual.First().Span.GetText().Length);
        }

        [TestMethod]
        public void GetClassificationSpans_OpenCloseTagDifferentLines_ReturnsBoth()
        {
            var source = "/* \r\n */";

            var actual = GetClassificationSpans(source);

            Assert.IsTrue(actual.Count == 1);
            Assert.AreEqual(source.Length, actual.First().Span.GetText().Length);
        }

        [TestMethod]
        public void GetClassificationSpans_OpenCloseTagDifferentLinesWithALineOfText_ReturnsBoth()
        {
            var source = "/* \r\ntest\r\n */";

            var actual = GetClassificationSpans(source);

            Assert.IsTrue(actual.Count == 1);
            Assert.AreEqual(source.Length, actual.First().Span.GetText().Length);
        }

        [TestMethod]
        public void GetClassificationSpans_OpenCloseTagDifferentLinesWithALineOfComment_ReturnsBoth()
        {
            var source = "/* \r\n//test\r\n */";

            var actual = GetClassificationSpans(source);

            Assert.IsTrue(actual.Count == 1);
            Assert.AreEqual(source.Length, actual.First().Span.GetText().Length);
        }

        [TestMethod]
        public void GetClassificationSpans_SingleLineComment_ReturnsAllAfter()
        {
            var source = "//test";

            var actual = GetClassificationSpans(source);

            Assert.IsTrue(actual.Count == 1);
            Assert.AreEqual(source.Length, actual.First().Span.GetText().Length);
        }

        [TestMethod]
        public void GetClassificationSpans_SpaceSingleLineComment_ReturnsAllAfter()
        {
            var source = " //test";

            var actual = GetClassificationSpans(source);

            Assert.IsTrue(actual.Count == 1);
            Assert.AreEqual(source.TrimStart().Length, actual.First().Span.GetText().Length);
        }

        [TestMethod]
        public void GetClassificationSpans_OpenCloseTagDifferentLinesSpace_ReturnsAllButSpace()
        {
            var source = "/* \r\n */ ";

            var actual = GetClassificationSpans(source);

            Assert.IsTrue(actual.Count == 1);
            Assert.AreEqual(source.Trim().Length, actual.First().Span.GetText().Length);
        }

        [TestMethod]
        public void GetClassificationSpans_SpaceOpenCloseTagDifferentLinesSpace_ReturnsAllButSpaces()
        {
            var source = " /* \r\n */ ";

            var actual = GetClassificationSpans(source);

            Assert.IsTrue(actual.Count == 1);
            Assert.AreEqual(source.Trim().Length, actual.First().Span.GetText().Length);
        }

        [TestMethod]
        public void GetClassificationSpans_NoMatch()
        {
            var source = "/ /";

            var actual = GetClassificationSpans(source);

            Assert.IsTrue(actual.Count == 0);
            
        }

      

       
     
       
       


    }

}
