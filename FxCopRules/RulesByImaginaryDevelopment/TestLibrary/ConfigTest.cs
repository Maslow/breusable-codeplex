﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace TestLibrary
{
    public class ConfigTest
    {
        public string TestProp { get { return System.Configuration.ConfigurationManager.AppSettings["test2"]; } }
        public string testField = System.Configuration.ConfigurationManager.AppSettings[0];
        public string NoUnderscore_Test { get; set; }
        string NoUnderscoreField_test;
        public static string TestStaticProp{get{return System.Configuration.ConfigurationManager.AppSettings["test1"];}}
        private static int TestStaticField = int.Parse(System.Configuration.ConfigurationManager.AppSettings[1]);
        public ConfigTest()
        {
            var setting = System.Configuration.ConfigurationManager.AppSettings[0];
            var test2 = System.Configuration.ConfigurationManager.AppSettings["test2"];
            var test3 = System.Configuration.ConfigurationSettings.AppSettings["test8"];
        }
        private void TestIndirection()
        {
            var settingsTest = "test9";
            var settings = System.Configuration.ConfigurationManager.AppSettings;

            Debug.Write(settings["test1"]);
            Debug.Write(settings[settingsTest]);
            Debug.WriteLine(settings[23]);
        }
    }
    public class DerivedConfigTest:ConfigTest
    {
        public string FalsePositiveCheck { get; set; }
        public string GetValue() { return string.Empty; }
        public string TestProperty { get; set; }
    }
}
