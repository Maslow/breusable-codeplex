﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace BReusable.Html
{
    /// Copyright 2010 Sedo Technologies, LLC (http://sedotech.com/)
    /// @author Sayed Ibrahim Hashimi
   public class FluentTagBuilder
    {
        public FluentTagBuilder(string tagName)
            : this(new TagBuilder(tagName))
        {
        }

        public FluentTagBuilder(TagBuilder tagBuilder)
        {
            this.TagBuilder = tagBuilder;
        }

        public TagBuilder TagBuilder
        { get; private set; }

        public IDictionary<string, string> Attributes
        {
            get
            {
                return this.TagBuilder.Attributes;
            }
        }

        public FluentTagBuilder AddAttribute(string key, string value)
        {
            this.TagBuilder.Attributes.Add(key, value);
            return this;
        }

        public string IdAttributeDotReplacement
        {
            get { return this.TagBuilder.IdAttributeDotReplacement; }
            set { this.TagBuilder.IdAttributeDotReplacement = value; }
        }

        public string InnerHtml
        {
            get { return this.TagBuilder.InnerHtml; }
            set { this.TagBuilder.InnerHtml = value; }
        }

        public string TagName
        {
            get { return this.TagBuilder.TagName; }
        }

        public FluentTagBuilder AddCssClass(string value)
        {
            this.TagBuilder.AddCssClass(value);
            return this;
        }

        public FluentTagBuilder SetInnerHtml(string innerHtml)
        {
            this.TagBuilder.InnerHtml = innerHtml;
            return this;
        }

        public FluentTagBuilder MergeAttributes(string key, string value, bool replaceExisting)
        {
            this.TagBuilder.MergeAttribute(key, value, replaceExisting);
            return this;
        }

        public FluentTagBuilder MergeAttributes<TKey, TValue>(IDictionary<TKey, TValue> attributes)
        {
            this.TagBuilder.MergeAttributes(attributes);
            return this;
        }

        public FluentTagBuilder MergeAttributes<TKey, TValue>(IDictionary<TKey, TValue> attributes, bool replaceExisting)
        {
            this.TagBuilder.MergeAttributes(attributes, replaceExisting);
            return this;
        }

        public FluentTagBuilder SetInnerText(string innerText)
        {
            this.TagBuilder.SetInnerText(innerText);
            return this;
        }

        public string ToString(TagRenderMode renderMode)
        {
            return this.TagBuilder.ToString(renderMode);
        }

        public override string ToString()
        {
            return this.TagBuilder.ToString();
        }

    }
}
