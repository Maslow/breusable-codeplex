﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.FxCop.Sdk;

namespace RulesByImaginaryDevelopment
{
    public class NoUnderscoresInProperties : BaseRule
    {
        public NoUnderscoresInProperties() : base("NoUnderscoresInProperties") { }
        
        public override ProblemCollection Check(Member member)
        {
            var prop = member as PropertyNode;
            if(prop==null)
                return Problems;
            if(prop.Name.Name.Contains("_"))
            {
                Problems.Add(new Problem(new Resolution("Remove any '_' from name "+prop.Name.Name)));
            }
            return Problems;
        }
    }
}
