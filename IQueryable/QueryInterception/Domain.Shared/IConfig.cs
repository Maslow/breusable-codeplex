﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace Domain
{
    public interface IConfig
    {
        [Key]
        int ConfigID { get; set; }
        [Required]
        string VarName { get; set; }
        string VarValue { get; set; }
        string Category { get; set; }

    }
}
