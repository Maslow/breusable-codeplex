﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HealthMonitor.Models;

namespace HealthMonitor.MvcControllers
{
    public class SiteController : Controller
    {
        protected override void OnResultExecuting(ResultExecutingContext filterContext)
        {
            base.OnResultExecuting(filterContext);
        }
        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            var needsSelectors = new[] { "Edit", "Create" };
            if (needsSelectors.Contains(filterContext.ActionDescriptor.ActionName) && filterContext.Result is ViewResult )
                PopulateEnvironments();
            base.OnActionExecuted(filterContext);
        }
      
        private HealthMonitorContext db = new HealthMonitorContext();

        //
        // GET: /Site/

        public ActionResult Index()
        {
            var model = db.Sites.ToArray();
            return View(model);
        }

        //
        // GET: /Site/Details/5

        public ActionResult Details(int id = 0)
        {
            Site site = db.Sites.Find(id);
            if (site == null)
            {
                return HttpNotFound();
            }
            return View(site);
        }

        //
        // GET: /Site/Create

        public ActionResult Create()
        {
            //PopulateEnvironments();
            return View();
        }

        void PopulateEnvironments()
        {
            var e = db.Environments.ToArray();
            ViewBag.Environments = e;
        }

        //
        // POST: /Site/Create

        [HttpPost]
        public ActionResult Create(Site site)
        {
            if (ModelState.IsValid)
            {
                db.Sites.Add(site);
                
                db.SaveChanges();
                return RedirectToAction("Index");
            }
           // PopulateEnvironments();
            return View(site);
        }

        //
        // GET: /Site/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Site site = db.Sites.Find(id);
            if (site == null)
            {
                return HttpNotFound();
            }
            //PopulateEnvironments();
            return View(site);
        }

        //
        // POST: /Site/Edit/5

        [HttpPost]
        public ActionResult Edit(Site site)
        {
            if (ModelState.IsValid)
            {
                db.Entry(site).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            //PopulateEnvironments();
            return View(site);
        }

        //
        // GET: /Site/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Site site = db.Sites.Find(id);
            if (site == null)
            {
                return HttpNotFound();
            }
            return View(site);
        }

        //
        // POST: /Site/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Site site = db.Sites.Find(id);
            db.Sites.Remove(site);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}