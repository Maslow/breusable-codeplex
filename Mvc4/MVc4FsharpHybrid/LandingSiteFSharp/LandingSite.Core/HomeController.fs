﻿namespace LandingSite.Controllers
open System
open System.Web
open System.Web.Mvc

[<AllowAnonymous>]
type HomeController() =
    inherit Controller()
    
    member this.Index() =
        this.ViewData.Add("Message","Modify this template to jump-start your ASP.NET MVC application.")
        this.View()
    member this.About() =
        this.ViewData.Add( "Message", "Your app description page.")
        this.View()
    member this.Contact() =
       this.ViewData.Add("Message", "Your contact page.") //http://stackoverflow.com/questions/8149127/set-a-property-on-viewbag-dynamic-object-in-f

