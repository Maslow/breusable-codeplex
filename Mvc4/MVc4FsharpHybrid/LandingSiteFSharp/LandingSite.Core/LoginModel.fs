﻿namespace LandingSite.Models
open System.ComponentModel.DataAnnotations

type LoginModel = {
    [<Required>]
    [<Display(Name="User name")>]
    UserName:string
    [<Required>]
    [<DataType(DataType.Password)>]
    [<Display(Name = "Password")>]
    Password:string
    [<Display(Name="Remember me?")>]
    RememberMe:bool
}

