﻿<#@ assembly name="System.Core" #>
<#@ import namespace="System.Collections.Generic" #>
<#@ Import Namespace="System.Linq" #>
<#@ include file="Validation.tt" #>
<#@ include file="MapperGenerator.tt" #>
<#+

    public class BusinessPropertyT4
    {
        private readonly string _typeString;
        public string TypeString { get { return _typeString; } }
      
        public BusinessPropertyT4(string type )
        {
            _typeString = type;
        }
        public string BoolValidation { get; set; }
        public string RuleViolationMessage { get; set; }
        public bool CopyExclude {get; set;}
    }
	
public class BusinessObjectTemplate : Template
{
	public bool UseStaticReflection{get;set;}
	public string CopyDictionaryNamespace{get;set;}
	public string RuleViolationNamespace{get;set;}
    public string BusinessName{get;set;}
  	public string ClassNameSpace{get;set;}
    public IDictionary<string,BusinessPropertyT4> PropertyList {get;set;}
	public bool GenerateValidationMethods{get;set;}
	public bool GenerateMapperMethods{get;set;}
	
    public override string TransformText()
    {
#>using System;
using System.Collections.Generic;

namespace <#=ClassNameSpace#>
{
	public class Model<#=BusinessName #>:I<#=BusinessName #>
	{
		
		#region I<#=BusinessName #> Members
		<#+
		PushIndent("\t");
		PushIndent("\t");
		foreach(string item in PropertyList.Keys)
		{
			WriteLine("public "+PropertyList[item].TypeString + " "+ item + " {get;set;}");
		}
		PopIndent();
		PopIndent();
		#>
#endregion 
		
<#+		 if(GenerateValidationMethods) { 
			var validation=new Validation(PropertyList,RuleViolationNamespace);
			WriteLine(validation.TransformText());
		}
		if(GenerateMapperMethods)
		{
			var mapper=new MapperGenerator(BusinessName,PropertyList,CopyDictionaryNamespace);
			WriteLine(mapper.TransformText());
		}
			#>
	} //end class
	
#region interface
	
	public interface I<#=BusinessName#>
	{
<#+
		PushIndent("\t");
		PushIndent("\t");
		foreach(string item in PropertyList.Keys)
		{
			WriteLine(PropertyList[item].TypeString+" "+item+" {get;set;}");
		}
		PopIndent();
		PopIndent();
		#>
	}
	
#endregion
	
}
	<#+ 
	return this.GenerationEnvironment.ToString();} }#>