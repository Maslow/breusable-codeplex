﻿using System;

namespace BReusable
{
	public class ErrorString
	{
		public String ErrorMessage { get; private set; }
		ErrorString(String errorMessage)
		{
			ErrorMessage = errorMessage;
		}
		public override string ToString()
		{
			return ErrorMessage;
		}
        //static public implicit operator String(ErrorString error)
        //{
        //    return error.ErrorMessage;
        //}
		public bool HasError { get { return ErrorMessage.IsNullOrEmpty()==false; } }
		static public implicit operator ErrorString(String errorMessage)
		{
			return new ErrorString(errorMessage);
		}
	}
}
