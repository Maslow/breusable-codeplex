﻿using System;
using System.Text;
using System.Windows.Forms;

namespace BReusable
{
	/// <summary>
	/// 
	/// </summary>
	/// <remarks>http://kasperbirch.wordpress.com/2007/12/12/redirecting-datacontextlog-to-textbox/</remarks>
	public class TextBoxWriter : System.IO.TextWriter
	{
		private Encoding _encoding;
		private TextBox textBox;
		

		public TextBoxWriter(TextBox textBox)
		{
			if (textBox == null)
				throw new NullReferenceException();
			this.textBox = textBox;
		}
		public override Encoding Encoding
		{
			get
			{
				if (this._encoding == null)
				{
					this._encoding = new UnicodeEncoding(false, false);
				}
				return _encoding;
			}
		}
		public override void Write(string value)
		{
			this.textBox.AppendText(value);
		}

		public override void Write(char[] buffer)
		{
			this.Write(new string(buffer));
		}
		public override void Write(char[] buffer, int index, int count)
		{
			this.Write(new string(buffer, index, count));
		}
	}  
}
