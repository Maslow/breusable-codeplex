using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.Text;
using Microsoft.VisualStudio.Text.Classification;
using Rhino.Mocks;

namespace T4EditorClassifier.Tests.Keyword {
	//[TestClass]
	public partial class T4KeywordClassifierTests {

		[TestMethod]
        public void GetClassificationSpans_Empty()
        {
            var mockregistry = MockRepository.GenerateStub<Microsoft.VisualStudio.Text.Classification.IClassificationTypeRegistryService>();
            mockregistry.Expect(r => r.GetClassificationType(string.Empty))
                .IgnoreArguments()
                .Return(MockRepository.GenerateStub<IClassificationType>());

            var c=new T4KeywordClassifier(mockregistry);
            var actual = c.GetClassificationSpans(new SnapshotSpan());
            Assert.IsNotNull(actual);
            Assert.IsFalse(actual.Any());
        }

        [TestMethod]
        public void GetClassificationSpans_ZeroLengthSnapShot()
        {
            var mockregistry=MockRepository.GenerateStub<Microsoft.VisualStudio.Text.Classification.IClassificationTypeRegistryService>();
            mockregistry.Expect(r => r.GetClassificationType(string.Empty))
                .IgnoreArguments()
                .Return(MockRepository.GenerateStub<IClassificationType>());

            var outputClassifier = new T4KeywordClassifier(mockregistry);
            var mockSnapshot = MockRepository.GenerateStub<ITextSnapshot>();
            mockSnapshot.Stub(s => s.Length).Return(0);
            var len = mockSnapshot.Length;
            var snapshotSpan = new SnapshotSpan(mockSnapshot, 0, 0);

            var actual=outputClassifier.GetClassificationSpans(snapshotSpan);
            
            Assert.IsNotNull(actual);
            Assert.IsFalse(actual.Any());
        }

		

		[TestMethod]
        public void GetClassificationSpans_ZeroLength()
        {
            var source = string.Empty;


            var actual = GetClassificationSpans(source);

            Assert.IsNotNull(actual);
            Assert.IsFalse(actual.Any());

        }

		IClassifier GetClassifier(IClassificationTypeRegistryService r)
        {
            return new T4KeywordClassifier(r);
        }

        IList<ClassificationSpan> GetClassificationSpans(string input)
        {
            return ClassificationTestHelper.GetClassificationSpans(input, GetClassifier);

        }
	}
}
