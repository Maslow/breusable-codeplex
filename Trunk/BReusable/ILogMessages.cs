﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BReusable
{
	public interface ILogMessages
	{
		string SaveMessage(string message, bool loanLevel);
		string SaveMessage(string message, bool loanLevel, Exception exception);
	}
}
