﻿namespace LandingSite.Controllers
open System
open System.Web
open System.Web.Mvc
open PaySpan.Core.WebServices
open PaySpan.Provider.Services
open LandingSite.Core
open Payformance.PaySpan.Contracts.Security

//http://tomasp.net/blog/fsharp-mvc-web.aspx
type PasswordController()= //securityServiceFactory:IWcfProxyFactory<IUserSecurityService>,registrationProxyFactory:IWcfProxyFactory<IRegistrationService>) =
    inherit Controller()
    let (|NonNull|_|) (a:Nullable<_>) =
        if a.HasValue then Some(a.Value) else None
    member this.Index() =
        this.View() :> ActionResult
    [<HttpGet>]
    [<AllowAnonymous>]
    member this.Forgot() = 
       this.ViewData.Model <- { UserId=0; ChallengeAnswer=String.Empty}
       this.View() :> ActionResult
        
        

