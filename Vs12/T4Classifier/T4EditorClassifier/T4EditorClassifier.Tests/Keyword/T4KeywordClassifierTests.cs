﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.Text;
using Microsoft.VisualStudio.Text.Classification;
using Rhino.Mocks;

namespace T4EditorClassifier.Tests.Keyword
{
    [TestClass]
    public partial class T4KeywordClassifierTests
    {
        protected const string boundaries = @"[{}]`~!@#$%^&*()-=+\/ ";
        
        [TestMethod]
        public void GetClassificationSpans_using_Returns1()
        {
            var source = "using";

            var actual = GetClassificationSpans(source);

            Assert.IsTrue(actual.Count == 1);
            Assert.AreEqual(source.Length, actual.First().Span.GetText().Length);
        }

        [TestMethod]
        public void GetClassificationSpans_BoundaryKeyword_ReturnsAllButBoundary()
        {
            foreach(var b in boundaries)
            foreach (var k in T4KeywordClassifier.Keywords)
            {
                var source = b + k;
                var actual = GetClassificationSpans(source);

                Assert.IsTrue(actual.Count == 1);
                Assert.AreEqual(source.Length-1, actual.First().Span.GetText().Length);
                Assert.AreEqual(k, actual.First().Span.GetText());
            }
        }

        [TestMethod]
        public void GetClassificationSpans_BoundaryKeywordBoundary_ReturnsAllButBoundaries()
        {
            foreach (var b in boundaries)
                foreach (var k in T4KeywordClassifier.Keywords)
                {
                    var source = b + k+b;
                    var actual = GetClassificationSpans(source);

                    Assert.IsTrue(actual.Count == 1);
                    Assert.AreEqual(source.Length - 2, actual.First().Span.GetText().Length);
                    Assert.AreEqual(k, actual.First().Span.GetText());
                }
        }

        [TestMethod]
        public void GetClassificationSpans_KeywordSpace_ReturnsAllButSpace()
        {
            foreach (var k in T4KeywordClassifier.Keywords)
            {
                var source = k + " ";
                var actual = GetClassificationSpans(source);

                Assert.IsTrue(actual.Count == 1);
                Assert.AreEqual(source.Trim().Length, actual.First().Span.GetText().Length);
            }

           
        }

        [TestMethod]
        public void GetClassificationSpans_NoMatch()
        {
            var source = "/ /";

            var actual = GetClassificationSpans(source);

            Assert.IsTrue(actual.Count == 0);

        }


        [TestMethod]
        public void GetClassificationSpans_SpaceKeywordSpace_ReturnsAllButSpaces()
        {
            foreach (var k in T4KeywordClassifier.Keywords)
            {
                var source = " " + k + " ";
                
                var actual = GetClassificationSpans(source);

                Assert.IsTrue(actual.Count == 1);
                Assert.AreEqual(source.Trim().Length, actual.First().Span.GetText().Length);
            }
        }

      

       


    }

}
