﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Threading;
using Roslyn.Compilers;
using Roslyn.Compilers.Common;
using Roslyn.Compilers.CSharp;
using Roslyn.Services;
using Roslyn.Services.Editor;

namespace SortMembers
{
    [ExportCodeRefactoringProvider("SortMembers", LanguageNames.CSharp)]
    public class CodeRefactoringProvider : ICodeRefactoringProvider
    {
        

        public CodeRefactoring GetRefactoring(IDocument document, TextSpan textSpan, CancellationToken cancellationToken)
        {
            // Retrieve the token containing textSpan
            var root = (SyntaxNode)document.GetSyntaxRoot(cancellationToken);
            var token = root.FindToken(textSpan.Start, findInsideTrivia: true);
            var parent = token.Parent;
            if (parent != null && (parent.Kind == SyntaxKind.ClassDeclaration || parent.Kind == SyntaxKind.StructDeclaration))
            {
                return new CodeRefactoring(new[]{new AutoArrangeCodeAction(document,parent as TypeDeclarationSyntax)});
            };
           
            return null;
        }
  //      [ImportingConstructor]
  //      public CodeRefactoringProvider(
  //  ICodeActionEditFactory editFactory)
  //{ 
  //  this.editFactory = editFactory;
  //} 
    }
}
