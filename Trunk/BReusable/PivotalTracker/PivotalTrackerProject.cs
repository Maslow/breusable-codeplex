﻿namespace BReusable.PivotalTracker
{
	public class PivotalTrackerProject
	{
		/*
			<project>
    <id>28228</id>
    <name>PivotalExporter</name>
    <iteration_length type="integer">1</iteration_length>
    <week_start_day>Monday</week_start_day>
    <point_scale>0,1,2,3</point_scale>
  </project> */
		public int Id { get; private set; }
		public string Name { get; private set; }
		public PivotalTrackerProject(int id, string name)
		{
			Id = id;
			Name = name;

		}

	}
}
