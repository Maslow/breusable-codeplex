﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataAccess.CodeFirst

{
    //[Table("Config")]
    public class Config
    {
        [Key]
        public int ConfigID { get; set; }
        public string VarName { get; set; }
        public string VarValue { get; set; }
        //public string Category { get; set; }
    }
}
