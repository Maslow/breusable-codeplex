﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using System.Web.Script.Serialization;

namespace HealthMonitor.Models
{
    [KnownType(typeof(Site))]
    public class Nvironment
    {
        [Key]

        [DefaultValue(0)]
        //[HiddenInput(DisplayValue = false)]
        //[UIHint("Hidden")]
        [ReadOnly(true)]
        [Editable(false)]
        public int NvironmentID { get; set; }

        [Unique]
        public string Name { get; set; }

        [ScriptIgnoreAttribute]
        public virtual ICollection<Site> Sites { get; set; }
    }
}