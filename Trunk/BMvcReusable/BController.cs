﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.Web.Routing;
using System.Linq.Expressions;
using BReusable.StaticReflection;

namespace BMvcReusable
{
    public class BController:Controller
    {

        /// <summary>
        /// BController Method
        /// </summary>
        /// <typeparam name="TController"></typeparam>
        /// <returns></returns>
        protected static string GetControllerName<TController>()
            where TController : Controller
        {
            return Extensions.ControllerName<TController>();
        }
        /// <summary>
        /// BController
        /// </summary>
        /// <typeparam name="TController"></typeparam>
        /// <param name="controllerActionExpression"></param>
        /// <returns></returns>
        protected static string GetControllerMethodName<TController>(
            Expression<Func<TController,ActionResult>> controllerActionExpression)
            where TController:Controller
        {
            return Member.MethodName(controllerActionExpression);
        }
        /// <summary>
        /// BController Method
        /// </summary>
        /// <typeparam name="TController"></typeparam>
        /// <param name="action"></param>
        /// <returns></returns>
        protected RedirectToRouteResult RedirectToAction<TController>(Expression<Func<TController, ActionResult>> actionExpression)
            where TController : Controller
        {
            return RedirectToAction(GetControllerMethodName<TController>(
                actionExpression),
                GetControllerName<TController>());

        }
        /// <summary>
        /// BController
        /// </summary>
        /// <typeparam name="TController"></typeparam>
        /// <param name="actionExpression"></param>
        /// <param name="values"></param>
        /// <returns></returns>
        protected RedirectToRouteResult RedirectToAction<TController>(Expression<Func<TController, ActionResult>> actionExpression,
            RouteValueDictionary values)
            where TController : Controller
        {
            return RedirectToAction(GetControllerMethodName(actionExpression),
               GetControllerName<TController>(), values);
        }

        /// <summary>
        /// BController
        /// </summary>
        /// <typeparam name="TController"></typeparam>
        /// <param name="actionExpression"></param>
        /// <param name="statusMessage"></param>
        /// <returns></returns>
        protected RedirectToRouteResult RedirectionToActionWithStatus<TController>(Expression<Func<TController, ActionResult>> actionExpression,
            string statusMessage,string tempDataStatusKey) where TController : Controller
        {
            TempData[tempDataStatusKey] = statusMessage;
            return RedirectToAction<TController>(actionExpression);
        }

    }
}
