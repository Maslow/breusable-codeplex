﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace QueryInterception
{
    class Profiler
    {
        /// <summary>
        /// Plug in your Profiler here
        /// </summary>
        /// <param name="interceptingQuery"></param>
        /// <returns></returns>
        public static IDisposable Step(string interceptingQuery) { return new Disposable(()=> { });}
    }
}
