﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace T4EditorClassifier
{
    public static class EnumerableExtensions
    {
        public static string DelimitBy(this IEnumerable<string> items, string delimiter)
        {
            return items.Aggregate((s1, s2) => s1 + delimiter + s2);
        }
    }
}
