﻿namespace LandingSite.Models
open System.ComponentModel.DataAnnotations

type RegisterModel = {
        [<Required>]
        [<Display(Name = "User name")>]
        UserName:string 

        [<Required>]
        [<StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)>]
        [<DataType(DataType.Password)>]
        [<Display(Name = "Password")>]
        Password:string

        [<DataType(DataType.Password)>]
        [<Display(Name = "Confirm password")>]
        [<Compare("Password", ErrorMessage = "The password and confirmation password do not match.")>]
        ConfirmPassword :string  
}

