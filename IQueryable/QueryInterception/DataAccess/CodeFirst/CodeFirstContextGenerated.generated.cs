﻿
using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using QueryInterception;

namespace DataAccess.CodeFirst
{
	public partial class CodeFirstContextGenerated : DbContext
				,IRepository<Config> 
	{
		public CodeFirstContextGenerated():base(){}
		public CodeFirstContextGenerated(string nameOrConnectionString):base(nameOrConnectionString){}
		//Config - Config
		protected DbSet<Config> Config {get;set;}
		protected Func<IQueryable<Config>,IQueryable<Config>> ConfigSelectHook {get;set;} 
		IQueryable<Config> IRepository<Config>.Query()
        {
            IQueryable<Config> q = from i in ((IObjectContextAdapter)this).ObjectContext.CreateObjectSet<Config>() select i;
			if(ConfigSelectHook!=null) q=ConfigSelectHook(q);
            return InterceptingProvider.Intercept(q, new ExpressionWriter(new DebugTextWriter()));
        }
	}
}