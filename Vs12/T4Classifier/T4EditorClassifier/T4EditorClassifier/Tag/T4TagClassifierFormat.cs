﻿using System.ComponentModel.Composition;
using System.Windows.Media;
using Microsoft.VisualStudio.Language.StandardClassification;
using Microsoft.VisualStudio.Text.Classification;
using Microsoft.VisualStudio.Utilities;

namespace T4EditorClassifier
{
    #region Format definition
    /// <summary>
    /// Defines an editor format for the T4EditorClassifier type that has a purple background
    /// and is underlined.
    /// </summary>
    [Export(typeof(EditorFormatDefinition))]
    [ClassificationType(ClassificationTypeNames =PredefinedClassificationTypeNames.Other)]
    [Name("T4TagClassifier")]
    [UserVisible(true)] //this should be visible to the end user
    [Order(Before = Priority.Default)]
    internal sealed class T4EditorClassifierFormat : ClassificationFormatDefinition
    {
        /// <summary>
        /// Defines the visual format for the "T4EditorClassifier" classification type
        /// </summary>
        public T4EditorClassifierFormat()
        {
            this.DisplayName = "T4TagClassifier"; //human readable version of the name
            this.BackgroundColor = Colors.Gold;
            //this.TextDecorations = System.Windows.TextDecorations.Underline;
        }
    }
    #endregion //Format definition
}
