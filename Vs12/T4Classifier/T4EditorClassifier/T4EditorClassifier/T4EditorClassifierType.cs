﻿using System.ComponentModel.Composition;
using Microsoft.VisualStudio.Text.Classification;
using Microsoft.VisualStudio.Utilities;

namespace T4EditorClassifier
{
    internal static class T4EditorClassifierClassificationDefinition
    {
        [Export]
        [Name("T4")]
        [BaseDefinition("plaintext")]
        internal static ContentTypeDefinition T4ContentTypeDefinition;

        /// <summary>
        /// Defines the "T4EditorClassifier" classification type.
        /// </summary>
        [Export(typeof(ClassificationTypeDefinition))]
        [FileExtension(".tt")]
        [Name("T4EditorClassifier")]
        [ContentType("text")]
        internal static ClassificationTypeDefinition T4EditorClassifierType = null;

        /// <summary>
        /// Defines the "T4EditorClassifier" classification type.
        /// </summary>
        [Export(typeof(ClassificationTypeDefinition))]
        [FileExtension(".tt")]
        [Name("T4EditorCommentClassifier")]
        [ContentType("text")]
        internal static ClassificationTypeDefinition T4EditorCommentClassifierType = null;

        /// <summary>
        /// Defines the "T4KeywordClassifier" classification type.
        /// </summary>
        [Export(typeof(ClassificationTypeDefinition))]
        [FileExtension(".tt")]
        [Name("T4KeywordClassifier")]
        [ContentType("text")]
        internal static ClassificationTypeDefinition T4KeywordClassifierType = null;


    }
}
