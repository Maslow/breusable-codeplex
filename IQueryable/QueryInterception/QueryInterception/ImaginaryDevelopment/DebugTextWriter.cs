﻿
using System;
using System.Text;
using System.IO;
using System.Diagnostics;

namespace QueryInterception
{

	public sealed class DebugTextWriter : StreamWriter
	{
		
		public DebugTextWriter(string category=null)
			: base(new DebugOutStream(category), Encoding.Unicode, 1024)
		{
			this.AutoFlush = true;
			
		}

		class DebugOutStream : Stream
		{
			readonly string _category = null;
			public DebugOutStream(string category)
			{
				_category = category;
			}
			public override void Write(byte[] buffer, int offset, int count)
			{
				if (String.IsNullOrEmpty(_category))
					Debug.Write(Encoding.Unicode.GetString(buffer, offset, count), _category);
				else
				Debug.Write(Encoding.Unicode.GetString(buffer, offset, count));
			}

			public override bool CanRead { get { return false; } }
			public override bool CanSeek { get { return false; } }
			public override bool CanWrite { get { return true; } }
			public override void Flush() { Debug.Flush(); }
			public override long Length { get { throw new InvalidOperationException(); } }
			public override int Read(byte[] buffer, int offset, int count) { throw new InvalidOperationException(); }
			public override long Seek(long offset, SeekOrigin origin) { throw new InvalidOperationException(); }
			public override void SetLength(long value) { throw new InvalidOperationException(); }
			public override long Position
			{
				get { throw new InvalidOperationException(); }
				set { throw new InvalidOperationException(); }
			}
		};
	}
}
