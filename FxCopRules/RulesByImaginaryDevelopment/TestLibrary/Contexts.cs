﻿

namespace System.Data.Linq
{
    public class DataContext
    {
        public string TestProperty { get; set; }
        public string GetTestString() { return string.Empty; }
    }
}
namespace System.Data.Objects
{
    public class ObjectContext
    {
        public string TestProperty { get; set; }
        public string GetTestString() { return string.Empty; }
    }
}
namespace System.Data.Entity
{
    public class DbContext
    {
        public string TestProperty { get; set; }
        public string GetTestString() { return string.Empty; }
    }
}