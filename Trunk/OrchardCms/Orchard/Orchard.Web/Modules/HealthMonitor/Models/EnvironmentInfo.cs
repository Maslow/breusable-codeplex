namespace HealthMonitor.Models {
    public class EnvironmentInfo {
        private readonly string _environment;
        private readonly UriInfo[] _uris;

        public string Environment {
            get { return _environment; }
        }

        public UriInfo[] Uris
        {
            get { return _uris; }
        }

        public EnvironmentInfo(string environment, UriInfo[] uris) {
            _environment = environment;
            _uris = uris;
        }
    }
}