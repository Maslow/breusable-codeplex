﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using HealthMonitor.Models;

namespace HealthMonitor.Controllers
{
    public class EnvironmentController : ApiController
    {
        private HealthMonitorContext db = new HealthMonitorContext();

        // GET api/Environment
        public IEnumerable<Nvironment> GetEnvironments()
        {
            return db.Environments.ToList();
        }

        // GET api/Environment/5
        public Nvironment GetEnvironment(int id)
        {
            Nvironment nvironment = db.Environments.Find(id);
            if (nvironment == null)
            {
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
            }

            return nvironment;
        }

        // PUT api/Environment/5
        public HttpResponseMessage PutEnvironment(int id, Nvironment nvironment)
        {
            if (ModelState.IsValid && id == nvironment.NvironmentID)
            {
                db.Entry(nvironment).State = EntityState.Modified;

                try
                {
                    db.SaveChanges();
                }
                catch (DbUpdateConcurrencyException)
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound);
                }

                return Request.CreateResponse(HttpStatusCode.OK);
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }

        // POST api/Environment
        public HttpResponseMessage PostEnvironment(Nvironment nvironment)
        {
            if (ModelState.IsValid)
            {
                if (db.Environments.Any(e => e.Name == nvironment.Name))
                    return Request.CreateResponse(HttpStatusCode.ExpectationFailed);
                db.Environments.Add(nvironment);
                try { db.SaveChanges(); }
                catch (DbUpdateException dbEx)
                {

                    return Request.CreateResponse(HttpStatusCode.Conflict);
                }

                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created, nvironment);
                response.Headers.Location = new Uri(Url.Link("DefaultApi", new { id = nvironment.NvironmentID}));
                return response;
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }

        // DELETE api/Environment/5
        public HttpResponseMessage DeleteEnvironment(int id)
        {
            Nvironment nvironment = db.Environments.Find(id);
            if (nvironment == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            db.Environments.Remove(nvironment);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            return Request.CreateResponse(HttpStatusCode.OK, nvironment);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}