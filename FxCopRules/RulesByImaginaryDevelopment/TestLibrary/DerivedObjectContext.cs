namespace TestLibrary
{
    public class DerivedObjectContext : System.Data.Objects.ObjectContext //System.Data.Entity.DbContext
    {
        public string GetValue() { return string.Empty; }
        public string TestProperty { get; set; }
        public string TestProperty2 { get; set; }
    }
}