﻿
//Project:BReusable

namespace BReusable.T4{
        
//Found item:BHttpLib.cs
//Found CodeModel:System.__ComObject
///CodeClass:BReusable.BHttpLib
public static class BHttpLib{
	public static string MetaName{get{return "BHttpLib";}}
	///CodeFunction:BReusable.BHttpLib.ConstructHttpRequest
	public static string ConstructHttpRequest{ get{ return "ConstructHttpRequest";}}
	///CodeFunction:BReusable.BHttpLib.ConstructHttpRequestXml
	public static string ConstructHttpRequestXml{ get{ return "ConstructHttpRequestXml";}}
	///CodeFunction:BReusable.BHttpLib.GetSneePostTest
	public static string GetSneePostTest{ get{ return "GetSneePostTest";}}
	///CodeFunction:BReusable.BHttpLib.GetRequestWithProxyBasicAuthentication
	public static string GetRequestWithProxyBasicAuthentication{ get{ return "GetRequestWithProxyBasicAuthentication";}}
	///CodeFunction:BReusable.BHttpLib.FromResponseGetString
	public static string FromResponseGetString{ get{ return "FromResponseGetString";}}
	///CodeFunction:BReusable.BHttpLib.FromRequestGetResponseString
	public static string FromRequestGetResponseString{ get{ return "FromRequestGetResponseString";}}
}

//Found item:BufferedLinqEntity2.cs
//Found CodeModel:System.__ComObject
///CodeClass:BReusable.BufferedLinqEntity2<TContext, TEntity>
public static class BufferedLinqEntity2{
	public static string MetaName{get{return "BufferedLinqEntity2";}}
	///CodeClass:BReusable.BufferedLinqEntity2<TContext, TEntity>.LiveObject
	public static class LiveObject{
		public static string MetaName{get{return "LiveObject";}}
		///CodeProperty:BReusable.BufferedLinqEntity2<TContext, TEntity>.LiveObject.DataContext
		public static string DataContext{ get{ return "DataContext";}}
		///CodeProperty:BReusable.BufferedLinqEntity2<TContext, TEntity>.LiveObject.Entity
		public static string Entity{ get{ return "Entity";}}
	}

	///CodeProperty:BReusable.BufferedLinqEntity2<TContext, TEntity>.IsNewAndUnattached
	public static string IsNewAndUnattached{ get{ return "IsNewAndUnattached";}}
	///CodeProperty:BReusable.BufferedLinqEntity2<TContext, TEntity>.HasChanges
	public static string HasChanges{ get{ return "HasChanges";}}
}

//Found item:Containers
//Found item:DebuggerWriter.cs
//Found CodeModel:System.__ComObject
///CodeClass:BReusable.DebuggerWriter
public static class DebuggerWriter{
	public static string MetaName{get{return "DebuggerWriter";}}
	///CodeFunction:BReusable.DebuggerWriter.Dispose
	public static string Dispose{ get{ return "Dispose";}}
	///CodeFunction:BReusable.DebuggerWriter.Write
	public static string Write{ get{ return "Write";}}
	///CodeProperty:BReusable.DebuggerWriter.Encoding
	public static string Encoding{ get{ return "Encoding";}}
	///CodeProperty:BReusable.DebuggerWriter.Level
	public static string Level{ get{ return "Level";}}
	///CodeProperty:BReusable.DebuggerWriter.Category
	public static string Category{ get{ return "Category";}}
}

//Found item:ErrorString.cs
//Found CodeModel:System.__ComObject
///CodeClass:BReusable.ErrorString
public static class ErrorString{
	public static string MetaName{get{return "ErrorString";}}
	///CodeProperty:BReusable.ErrorString.ErrorMessage
	public static string ErrorMessage{ get{ return "ErrorMessage";}}
	///CodeFunction:BReusable.ErrorString.ToString
	public static string ToString{ get{ return "ToString";}}
	///CodeProperty:BReusable.ErrorString.HasError
	public static string HasError{ get{ return "HasError";}}
}

//Found item:Excel.cs
//Found CodeModel:System.__ComObject
///CodeClass:BReusable.Excel
public static class Excel{
	public static string MetaName{get{return "Excel";}}
	///CodeFunction:BReusable.Excel.GetSheets
	public static string GetSheets{ get{ return "GetSheets";}}
	///CodeFunction:BReusable.Excel.ExcelColumn
	public static string ExcelColumn{ get{ return "ExcelColumn";}}
}

//Found item:Extensions
//Found item:ILogMessages.cs
//Found CodeModel:System.__ComObject
///CodeIntrface:BReusable.ILogMessages
		public static class ILogMessages{
			public static string MetaName{get{return "ILogMessages";}}
			///CodeFunction:BReusable.ILogMessages.SaveMessage
public static string SaveMessage{ get{ return "SaveMessage";}}
		}
		
		//Found item:Linq.cs
//Found CodeModel:System.__ComObject
///CodeClass:BReusable.Linq
public static class Linq{
	public static string MetaName{get{return "Linq";}}
	///CodeFunction:BReusable.Linq.GetColumnAttributes
	public static string GetColumnAttributes{ get{ return "GetColumnAttributes";}}
	///CodeFunction:BReusable.Linq.GetColumnAttribute<TEntity>
	public static string GetColumnAttribute{ get{ return "GetColumnAttribute";}}
}

//Found item:LinqLib.cs
//Found CodeModel:System.__ComObject
///CodeClass:BReusable.LinqLib
public static class LinqLib{
	public static string MetaName{get{return "LinqLib";}}
	///CodeFunction:BReusable.LinqLib.Using<TDataContext>
	public static string Using{ get{ return "Using";}}
	///CodeFunction:BReusable.LinqLib.GetColumnNames
	public static string GetColumnNames{ get{ return "GetColumnNames";}}
	///CodeFunction:BReusable.LinqLib.GetLengthLimit
	public static string GetLengthLimit{ get{ return "GetLengthLimit";}}
}

//Found item:Pair.cs
//Found CodeModel:System.__ComObject
///CodeClass:BReusable.Pair<TValue1, TValue2>
public static class Pair{
	public static string MetaName{get{return "Pair";}}
	///CodeProperty:BReusable.Pair<TValue1, TValue2>.Value2
	public static string Value2{ get{ return "Value2";}}
}

//Found item:PivotalTracker
//Found item:Properties
//Found item:RandomKeyGenerator.cs
//Found CodeModel:System.__ComObject
///CodeClass:BReusable.RandomKeyGenerator
public static class RandomKeyGenerator{
	public static string MetaName{get{return "RandomKeyGenerator";}}
	///CodeFunction:BReusable.RandomKeyGenerator.Generate
	public static string Generate{ get{ return "Generate";}}
	///CodeFunction:BReusable.RandomKeyGenerator.RandomNumber
	public static string RandomNumber{ get{ return "RandomNumber";}}
}

//Found item:Singleton.cs
//Found CodeModel:System.__ComObject
///CodeClass:BReusable.Singleton<T>
public static class Singleton{
	public static string MetaName{get{return "Singleton";}}
	///CodeProperty:BReusable.Singleton<T>.Value
	public static string Value{ get{ return "Value";}}
	///CodeFunction:BReusable.Singleton<T>.ToString
	public static string ToString{ get{ return "ToString";}}
}

//Found item:StaticReflection
//Found item:T4
//Found item:TryResult.cs
//Found CodeModel:System.__ComObject
///CodeClass:BReusable.BResult<T>
public static class BResult{
	public static string MetaName{get{return "BResult";}}
	///CodeProperty:BReusable.BResult<T>.ErrorMessage
	public static string ErrorMessage{ get{ return "ErrorMessage";}}
	///CodeProperty:BReusable.BResult<T>.Result
	public static string Result{ get{ return "Result";}}
}

///CodeClass:BReusable.TryAction
public static class TryAction{
	public static string MetaName{get{return "TryAction";}}
	///CodeProperty:BReusable.TryAction.Exception
	public static string Exception{ get{ return "Exception";}}
}

///CodeClass:BReusable.TryFunc<T>
public static class TryFunc{
	public static string MetaName{get{return "TryFunc";}}
	///CodeProperty:BReusable.TryFunc<T>.Result
	public static string Result{ get{ return "Result";}}
	///CodeProperty:BReusable.TryFunc<T>.HasValue
	public static string HasValue{ get{ return "HasValue";}}
}

//Found item:UnitTesting.cs
//Found CodeModel:System.__ComObject
///CodeClass:BReusable.UnitTesting
public static class UnitTesting{
	public static string MetaName{get{return "UnitTesting";}}
	///CodeFunction:BReusable.UnitTesting.HasPermission
	public static string HasPermission{ get{ return "HasPermission";}}
	///CodeFunction:BReusable.UnitTesting.Has_Perms_By_Name
	public static string Has_Perms_By_Name{ get{ return "Has_Perms_By_Name";}}
}

//Found item:XmlFont.cs
//Found CodeModel:System.__ComObject
///CodeClass:XmlFontExtensions
public static class XmlFontExtensions{
	public static string MetaName{get{return "XmlFontExtensions";}}
	///CodeFunction:XmlFontExtensions.ToXmlFont
	public static string ToXmlFont{ get{ return "ToXmlFont";}}
}


}

