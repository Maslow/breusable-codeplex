
//Host is Microsoft.VisualStudio.TextTemplating.VSHost.TextTemplatingService
using System;
using System.Collections.Generic;

namespace TestNamespace
{
	public class ModelRegistration:IRegistration
	{
		
		#region IRegistration Members
		public string UserName {get;set;}
		public string Name {get;set;}
		public string Email {get;set;}
		public string MailCode {get;set;}
		public string TelephoneNumber {get;set;}
		public int? OrganizationId {get;set;}
		public int? OrganizationSponsorId {get;set;}
#endregion 
		
#region ValidationCode

		public IEnumerable<BReusable.T4.RuleViolation> GetRuleViolations()
        {
        	if (String.IsNullOrEmpty(UserName))
				 yield return new BReusable.T4.RuleViolation("UserName is required","UserName");
	 		//if (Latitude ==0 || Longitude == 0)
			// yield return new RuleViolation("Make sure to enter a valid address","Address");
			yield break;
		}
		
#endregion ValidationCode


#region MappingMethods
			
			public static Dictionary<string,Action> GenerateActionDictionary<T>(T dest, IRegistration source, bool includeIdentifier)
				where T: IRegistration
				{
					var result= new Dictionary<string,Action>()
						{
							{"UserName",()=>dest.UserName=source.UserName},
							{"Name",()=>dest.Name=source.Name},
							{"Email",()=>dest.Email=source.Email},
							{"MailCode",()=>dest.MailCode=source.MailCode},
							{"TelephoneNumber",()=>dest.TelephoneNumber=source.TelephoneNumber},
							{"OrganizationId",()=>dest.OrganizationId=source.OrganizationId},
							{"OrganizationSponsorId",()=>dest.OrganizationSponsorId=source.OrganizationSponsorId},
						};
			return result;}
			
			public static T CopyData<T>(Func<T> creator, IRegistration source, bool includeIdentifier,
			ICollection<string> excludeList) where T: IRegistration
			{
				return BReusable.T4.CopyDictionary<T,IRegistration>.CopyData(GenerateActionDictionary,creator,source,includeIdentifier, excludeList);
			}
		
#endregion MappingMethods


	} //end class
	
#region interface
	
	public interface IRegistration
	{
		string UserName {get;set;}
		string Name {get;set;}
		string Email {get;set;}
		string MailCode {get;set;}
		string TelephoneNumber {get;set;}
		int? OrganizationId {get;set;}
		int? OrganizationSponsorId {get;set;}
	}
	
#endregion
	
}
	