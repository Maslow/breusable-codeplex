﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
//using System.Web.Mvc;

namespace HealthMonitor.Models
{
    [KnownType(typeof(Nvironment))]
    public class Site
    {
        [Key]
        //[DefaultValue(0)]
        //[HiddenInput(DisplayValue = false)]
        //[UIHint("Hidden")]
        //[ReadOnly(true)]
        //[Editable(false)]

        public int SiteID { get; set; }

        [Unique]
        public string Name { get; set; }

        public string Uri { get; set; }
        [DisplayName("Environment")]
        [UIHint("Nvironment")]
        public int? NvironmentID { get; set; }

        //[DisplayName("Environment")]
        //[ForeignKey("EnvironmentID")]
        //public virtual Nvironment Nvironment { get; set; }
    }
}