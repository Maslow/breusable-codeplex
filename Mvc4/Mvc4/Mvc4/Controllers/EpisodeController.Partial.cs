﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Mvc4.Controllers
{
    public partial class EpisodeController
    {
        partial void PopulateViewBag()
        {
            ViewBag.ShowSelect = db.Shows.OrderBy(s=>s.ShowName).ToArray().Select(s => new SelectListItem { Text = s.ShowName, Value = s.ShowID.ToString() }).ToArray();

        }
        public ActionResult RunComplexQuery()
        {
            var q = from s in db.Shows
                    join e in db.Episodes
                    on s.ShowID equals e.ShowID
                    select new { e.EpisodeName, ComplexResult = e.EpisodeID * s.ShowID };
            var result=db.Insert(q,db.Episodes);
            
            return Json(result, JsonRequestBehavior.AllowGet);

        }
    }
}