﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Linq;

namespace BReusable.Extensions.HtmlExtensions
{
	public static class HtmlExtensions
	{
		public const String HtmlHeader = "<meta http-equiv=\"Content-Type\" content=\"text/html;charset=utf-8\" >";
		/// <summary>
		/// Wraps the text in an html tag like < > and </ >
		/// From BReusable
		/// </summary>
		/// <param name="text"></param>
		/// <param name="tag">For example "html", "table", "tr"</param>
		/// <returns></returns>
		public static String WrapInHtmlTag(this String text, String tag)
		{
			return "<" + tag + ">" + text + "</" + tag + ">";
		}
		public static StringBuilder WrapInHtmlTag(this StringBuilder sb, string tag,string text)
		{
			sb.AppendLine(text.WrapInHtmlTag(tag));
			return sb;
		}
		/// <summary>
		/// Reflects on an object 
		/// </summary>
		/// <param name="objectToReflect"></param>
		/// <returns></returns>
		public static String ToHtmlTable(this object objectToReflect)
		{
			var result = new StringBuilder();
			result.AppendLine("<table border=\"1\" cellpadding=\"5\">");
			objectToReflect.ReflectProperties()
				.ForEach(propertyInfo =>
					{
						if (propertyInfo.PropertyType.FullName == typeof(string).FullName)
						{
							var value = propertyInfo.GetValue(objectToReflect, null);
							result.AppendLine(
								(propertyInfo.Name.WrapInHtmlTag("td")
								 + (value == null ? string.Empty : value.ToString()).WrapInHtmlTag("td")
								).WrapInHtmlTag("tr")
								);
						}
					});
			result.AppendLine("</table><br />");
			
			return result.ToString();
		}

	//    public static string ToHtmlTableWithTr<T>(this IEnumerable<T> list,string cssClass,Func<T,String> rowHtmlFunc)
	//{
	//        return ToHtmlTable(list, cssClass, row => rowHtmlFunc(row).WrapInHtmlTag("tr"));
	//}
	

		
		

		//public static string ToHtmlTable<T>(this IEnumerable<T> list, string cssClass,string rowClass,
		//    string rowAlternatingClass, Func<T, String> rowHtmlFuncWithoutTr)
		//{
		
		//    var count = 0;
		//    var sbResult = new StringBuilder(HtmlOpenTag("table", "class=\"" + cssClass ?? string.Empty + "\""));
		
		//    foreach (var item in list)
		//    {
		//        var tr=new XElement("tr",new XAttribute("class",count.IsEven()?rowClass:rowAlternatingClass));
		//        count++;
		//        System.Windows.Forms.MessageBox.Show("test");
		//        xml.Add(XElement.Parse("<tr class="+(count.IsEven()?rowClass:rowAlternatingClass)+">"+
		//            rowHtmlFuncWithoutTr(item)+"</tr>"));
				
		//    }
		//    return "<table" + (cssClass != null ? " class=\"" + cssClass + "\"" :
		//        String.Empty) + ">"+ xml.ToString()+"</table>";
			
		
		//}
		/// <summary>
		/// Uses reflection to write the properties of an object out as a table row in html
		/// </summary>
		/// <param name="objectToReflect"></param>
		/// <returns></returns>
		public static String ToHtmlRow(this object objectToReflect)
		{
			var result = new StringBuilder();
			result.Append("<tr>");

			objectToReflect.ReflectProperties().ForEach((propertyInfo) =>
															{
																if (propertyInfo.PropertyType.FullName == typeof(string).FullName)
																	result.AppendLine(propertyInfo.Name.WrapInHtmlTag("td")
																					  + propertyInfo.GetValue(objectToReflect, null).ToString().WrapInHtmlTag("td"));
															});
			result.AppendLine("</tr>");
			return result.ToString();

		}

	}
}