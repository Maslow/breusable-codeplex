﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;

namespace Mvc4.Models.Utilities
{
    /// <summary>
    /// http://forums.asp.net/t/997608.aspx/1
    /// </summary>
    public class MvcMembershipProvider : SqlMembershipProvider
    {
        public MvcMembershipProvider()
            : base()
        {
            FixConnectionString();
        }
        private void FixConnectionString()
        {
            // Update the private connection string field in the base class.
            var connectionString = SqlConnectionStringManager.ConnectionString;

            // Set private property of Membership provider.
            var connectionStringField = GetType().BaseType.GetField("_sqlConnectionString",
                System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic);
            connectionStringField.SetValue(this, connectionString);
        }
        public override void Initialize(string name, System.Collections.Specialized.NameValueCollection config)
        {
            base.Initialize(name, config);
            FixConnectionString();

        }
    }
}