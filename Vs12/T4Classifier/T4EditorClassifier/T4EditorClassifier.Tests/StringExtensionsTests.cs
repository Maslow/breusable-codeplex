﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace T4EditorClassifier.Tests
{
    [TestClass]
    public class StringExtensionsTests
    {
        [TestMethod]
        public void SplitPreservingLines_EmptyString_ReturnsSingleEmpty()
        {
            var s = string.Empty;
            var actual = s.SplitLinesPreserve();
            Assert.IsNotNull(actual);
            Assert.IsTrue(actual.Length == 1);
            Assert.AreEqual(s, actual.First());
        }

        [TestMethod]
        public void SplitPreservingLines_Whitespace_ReturnsSingleWithAllWhitespace()
        {
            var s = " \t";
            var actual = s.SplitLinesPreserve();
            Assert.IsNotNull(actual);
            Assert.IsTrue(actual.Length == 1);
            Assert.AreEqual(s, actual.First());
        }

        [TestMethod]
        public void SplitPreservingLines_WhitespaceWithEnd_Returns1WithAllWhitespace2Empty()
        {
            var s = " \t\r\n"; //a \n counts as an end of line, so line 2 is empty
            var actual = s.SplitLinesPreserve();
            Assert.IsNotNull(actual);
            Assert.IsTrue(actual.Length == 2);
            Assert.AreEqual(s, actual.First());
            Assert.AreEqual(string.Empty, actual.Skip(1).First());
        }

        [TestMethod]
        public void SplitPreservingLines_JustReturn_ReturnsAllChars()
        {
            var s = " \t\r"; //a \n counts as an end of line, so line 2 is empty
            var actual = s.SplitLinesPreserve();
            Assert.IsNotNull(actual);
            Assert.IsTrue(actual.Length == 1);
            Assert.AreEqual(s, actual.First());
          
        }

    }

}
