using System;

namespace HealthMonitor.Models {
    public class UriInfo {
        private readonly string _name;
        public String Uri { get { return _uri.ToString(); }
        }
        private readonly string _status;
        private Uri _uri;

        public string Name {
            get { return _name; }
        }

        public string Status {
            get { return _status; }
        }

        public UriInfo(string name,Uri uri, string status) {
            _name = name;
            _uri = uri;
            _status = status;
        }
    }
}