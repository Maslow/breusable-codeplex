﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace T4EditorClassifier
{
    public static class StringExtensions
    {
        public static bool IsNullOrEmpty(this string s)
        {
            return string.IsNullOrEmpty(s);
        }
        public static bool HasValue(this string s)
        {
            return !s.IsNullOrEmpty();
        }
        public static string[] SplitLinesPreserve(this string s)
        {
            //"test\r\n heilo\rbye\n "
            var result=Regex.Matches(s,@"^.*?(\n|$)", RegexOptions.Multiline).Cast<Match>().Select(m=>m.Value).ToArray();
            return result;
            
        } 
    }
}
