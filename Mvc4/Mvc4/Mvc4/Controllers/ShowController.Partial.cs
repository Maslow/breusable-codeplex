﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Mvc4.Models;
using Mvc4.Models.EF;

namespace Mvc4.Controllers
{
    public  partial class ShowController
    {
        public virtual ActionResult Details(int id)
        {
            
            var show=db.Shows.FirstOrDefault(s => s.ShowID == id);
            if (show == null)
                return RedirectToAction(MVC.Show.Index());
            var episodes = db.Episodes.Where(e => e.ShowID == id).ToArray();
            return View(MVC.Show.Views.Details,Tuple.Create<ShowModel,IEnumerable<EpisodeModel>>(show,episodes));
        }
        
    }
}